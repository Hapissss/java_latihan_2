package com.tugasHafizh;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        // Ini ArrayList
        // ArrayList<String> collection1 = new ArrayList<>();
        // collection1.add("Risa");
        // collection1.add("Reza");
        // collection1.add("Liana");

        // for (String name : collection1) {
        //     System.out.println("Nama: " + name);
        // }


        // // Menggunakan Iterator dan While
        // Iterator itrCollection1 = collection1.iterator();
        // while (itrCollection1.hasNext()){
        //     System.out.println("Nama: " + itrCollection1.next());
        // }

        // ArrayList<String> bagianA = new ArrayList<>();
        // bagianA.add("Anggi");
        // bagianA.add("Joko");

        // ArrayList<String> bagianB = new ArrayList<>();
        // bagianB.add("Anggi");
        // bagianB.add("Joko");
        // bagianB.add("Lolo");
        
        // ArrayList<String> newBagianA = new ArrayList<>();
        // newBagianA.addAll(bagianB);

        // ArrayList<String> newBagianB = new ArrayList<>();

        // Iterator itrBagianA = bagianA.iterator();
        // while(itrBagianA.hasNext()){
        //     System.out.println(itrBagianA.next());
        // }
        // System.out.println(bagianA);
        // System.out.println(bagianB);

        // Iterator itrBagianB = bagianB.iterator();
        // while(itrBagianB.hasNext()){
        //     System.out.println(itrBagianB.next());
        // }
        // bagianB.retainAll(bagianA);
        // System.out.println(bagianA);
        ArrayList<String> furniture = new ArrayList<>();
        furniture.add("Kursi");
        furniture.add("Lemari");
        furniture.add("Meja");

        ArrayList<String> furnitureKamar = new ArrayList<>();
        furnitureKamar.add("Meja");
        furnitureKamar.add("Tempat Tidur");

        ArrayList<String> newFurniture = new ArrayList<String>(furniture);
        newFurniture.addAll(furnitureKamar);

        ArrayList<String> substract = new ArrayList<String>(furniture);
        substract.retainAll(furnitureKamar);

        newFurniture.removeAll(substract);            

	    furniture.removeAll(furniture);
        furniture.addAll(newFurniture);
        System.out.println(furniture);

        // DIBANTU DENGAN TUGAS FELIX
        // Alur sudah mengerti kak.
    }
}